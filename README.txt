INTRODUCTION
------------
Seamless Presentation is an e-learning content authoring tool.
There are 3 sub-modules:
* seamless_slides
  This module manages the seamless content types and edit forms.
* seamless_viewer
  This module provides the presentation layer, the communication layer 
  (web services API), and student progress data.
* seamless_reports
  This module adds student progress report screens for teachers.

REQUIREMENTS
------------
This module requires the following modules:
* ctools, page_manager
* panels
* paragraphs
* entityreference, entityreference_view_widget
* features
* field_group
* markup
* media, file_entity
* views
* services, rest_server

RECOMMENDED MODULES
-------------------
* media_wysiwyg with ckeditor
  Whith this module it is possible to display text readability statistics for
  text fields in the Seamless edit forms.
* closedquestion
  The Seamless module can embed closedquestions in slides when this module is 
  enabled.

INSTALLATION
------------
* Install as you would normally install a contributed Drupal module. Visit
  https://www.drupal.org/documentation/install/modules-themes/modules-7
  for further information.

Enable the seamless_slides and seamless_viewer submodules. The seamless_reports
module is optional, and can be enabled later.

CONFIGURATION
-------------
* Configure the user permissions in Administration » People » Permissions:
  - Allow seamless qrcode login
    User with this permission can use a qrcode to open a Seamless presentation
    or slide on a mobile device.
  - Make sure that roles that are allowed to create or edit Seamless content
    have permission to create and edit the content types "Seamless Presentation"
    and "Seamless Slides".
  - Media are added to the Seamless slides using the Media module. The
    configuration of the media module determines what media types can be added
    and how they are displayed.
