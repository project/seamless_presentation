<?php

/**
 * @file
 * seamless_viewer.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function seamless_viewer_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance:
  // 'seamless_slidedata-seamless_slidedata-field_arcs_a'.
  $field_instances['seamless_slidedata-seamless_slidedata-field_arcs_a'] = array(
    'bundle' => 'seamless_slidedata',
    'default_value' => array(
      0 => array(
        'value' => 0,
      ),
    ),
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'number',
        'settings' => array(
          'decimal_separator' => '.',
          'prefix_suffix' => TRUE,
          'scale' => 0,
          'thousand_separator' => '',
        ),
        'type' => 'number_integer',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'seamless_slidedata',
    'field_name' => 'field_arcs_a',
    'label' => 'ARCS A',
    'required' => 0,
    'settings' => array(
      'max' => 5,
      'min' => 0,
      'prefix' => '',
      'suffix' => '',
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 0,
      'module' => 'number',
      'settings' => array(),
      'type' => 'number',
      'weight' => 1,
    ),
  );

  // Exported field_instance:
  // 'seamless_slidedata-seamless_slidedata-field_arcs_c'.
  $field_instances['seamless_slidedata-seamless_slidedata-field_arcs_c'] = array(
    'bundle' => 'seamless_slidedata',
    'default_value' => array(
      0 => array(
        'value' => 0,
      ),
    ),
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'number',
        'settings' => array(
          'decimal_separator' => '.',
          'prefix_suffix' => TRUE,
          'scale' => 0,
          'thousand_separator' => '',
        ),
        'type' => 'number_integer',
        'weight' => 2,
      ),
    ),
    'entity_type' => 'seamless_slidedata',
    'field_name' => 'field_arcs_c',
    'label' => 'ARCS C',
    'required' => 0,
    'settings' => array(
      'max' => 5,
      'min' => 0,
      'prefix' => '',
      'suffix' => '',
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 0,
      'module' => 'number',
      'settings' => array(),
      'type' => 'number',
      'weight' => 3,
    ),
  );

  // Exported field_instance:
  // 'seamless_slidedata-seamless_slidedata-field_arcs_r'.
  $field_instances['seamless_slidedata-seamless_slidedata-field_arcs_r'] = array(
    'bundle' => 'seamless_slidedata',
    'default_value' => array(
      0 => array(
        'value' => 0,
      ),
    ),
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'number',
        'settings' => array(
          'decimal_separator' => '.',
          'prefix_suffix' => TRUE,
          'scale' => 0,
          'thousand_separator' => '',
        ),
        'type' => 'number_integer',
        'weight' => 1,
      ),
    ),
    'entity_type' => 'seamless_slidedata',
    'field_name' => 'field_arcs_r',
    'label' => 'ARCS R',
    'required' => 0,
    'settings' => array(
      'max' => 5,
      'min' => 0,
      'prefix' => '',
      'suffix' => '',
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 0,
      'module' => 'number',
      'settings' => array(),
      'type' => 'number',
      'weight' => 2,
    ),
  );

  // Exported field_instance:
  // 'seamless_slidedata-seamless_slidedata-field_arcs_s'.
  $field_instances['seamless_slidedata-seamless_slidedata-field_arcs_s'] = array(
    'bundle' => 'seamless_slidedata',
    'default_value' => array(
      0 => array(
        'value' => 0,
      ),
    ),
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'number',
        'settings' => array(
          'decimal_separator' => '.',
          'prefix_suffix' => TRUE,
          'scale' => 0,
          'thousand_separator' => '',
        ),
        'type' => 'number_integer',
        'weight' => 3,
      ),
    ),
    'entity_type' => 'seamless_slidedata',
    'field_name' => 'field_arcs_s',
    'label' => 'ARCS S',
    'required' => 0,
    'settings' => array(
      'max' => 5,
      'min' => 0,
      'prefix' => '',
      'suffix' => '',
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 0,
      'module' => 'number',
      'settings' => array(),
      'type' => 'number',
      'weight' => 4,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('ARCS A');
  t('ARCS C');
  t('ARCS R');
  t('ARCS S');

  return $field_instances;
}
