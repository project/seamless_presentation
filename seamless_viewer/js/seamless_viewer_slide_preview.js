(function ($) {
  Drupal.behaviors.seamless_viewer_slide_preview = {
    attach: function (context, settings) {
      // Resize timeout event ID.
      var resizeTimeoutId;
      // Resize timeout duration in milliseconds.
      var resizeTimeoutDuration = 100;

      /**
       * Debounces resizing behaviors.
       */
      var resizeDebounce = function () {
        clearTimeout(resizeTimeoutId);
        resizeTimeoutId = setTimeout(doneResizing, resizeTimeoutDuration);
      };

      /**
       * Main screen resize logic.
       *
       * This function is called after the screen is finished resizing.
       */
      var doneResizing = function () {
        var $slideFrame = $('iframe#slide-preview');
        // Make the seamless preview iframe fill the rest of the epage.
        var frameHeight = window.innerHeight - $slideFrame.offset().top;
        $slideFrame.attr('height', frameHeight);
        // Hide scrollbars.
        $('body').css('overflow', 'hidden');
        window.scrollTo(0, 0);
      };

      /**
       * Window resize events.
       */
      $(window).resize(resizeDebounce);
      resizeDebounce();
      $(window).unload(function () {
        $('iframe').remove();
      });
    }
  };

})(jQuery);
