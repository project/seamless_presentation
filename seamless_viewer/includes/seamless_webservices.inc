<?php

/**
 * @file
 * Include file for handling webservices callbacks.
 */

/**
 * Callback function for seamless presentation retrieve.
 *
 * Return an array of seamless_presentation nodes.
 * Can optionally be paged an based on a set of selection criteria.
 *
 * @param array $page
 *   Integer page number we are requesting.
 * @param array $fields
 *   Array fields to return.
 * @param array $parameters
 *   Array parameters to add to the index query.
 * @param int $page_size
 *   Integer number of items to be returned.
 *
 * @return array
 *   List of presentations.
 */
function _ws_seamless_viewer_presentation_index($page, $fields, $parameters, $page_size) {
  module_load_include('inc', 'services', 'services.module');
  $node_select = db_select('node', 't')
    ->condition('type', 'seamless_presentation', '=')
    ->addTag('node_access')
    ->orderBy('created', 'ASC');

  services_resource_build_index_query($node_select, $page, $fields, $parameters, $page_size, 'node');

  if (!user_access('administer nodes')) {
    $node_select->condition('status', 1);
  }

  $results = services_resource_execute_index_query($node_select);

  return services_resource_build_index_list($results, 'seamless_presentation', 'nid');
}

/**
 * Return Seamless slide with id $nid.
 *
 * @param int $nid
 *   Seamless slide node id.
 */
function _ws_seamless_viewer_slide_retrieve($nid) {
  $nid = intval($nid);

  $node = clone node_load($nid);

  if ($node && $node->type == 'slide_text_image') {
    $slide_wrapper = entity_metadata_wrapper('node', $node);
    $slide = seamless_viewer_get_slide($slide_wrapper);
    return $slide;
  }
  else {
    return services_error(t('Wrong node type.'), 406);
  }
}

/**
 * Return seamless presentation with id $nid.
 *
 * @param int $pid
 *   Presentation node id.
 *
 * @return object
 *   Presentation object.
 */
function _ws_seamless_viewer_presentation_retrieve($pid) {
  global $user;
  $uid = $user->uid;
  $pid = intval($pid);

  $node = clone node_load($pid);

  if ($node && $node->type == 'seamless_presentation') {
    $wrapper = entity_metadata_wrapper('node', $node);
    $presentation = new stdClass();
    $presentation->title = $wrapper->label();
    $presentation->slides = array();
    $presentation->total_weight = 0;
    $presentation->completion_options = seamless_viewer_completion_options();
    $presentation->allow_qrcode_login = seamless_viewer_allow_url_login();
    unset($presentation->completion_options[0]);

    if (isset($wrapper->field_enable_student_arcs_rating)) {
      $arcs_enabled = $wrapper->field_enable_student_arcs_rating->value();
      $presentation->arcs_enabled = ($arcs_enabled == "1");
    }
    if (isset($wrapper->field_seamless_slides)) {
      foreach ($wrapper->field_seamless_slides as $slide_wrapper) {
        // Retrieve slide.
        $slide = seamless_viewer_get_slide($slide_wrapper);
        // Add slide data.
        $sid = $slide_wrapper->getIdentifier();
        $presentation->total_weight += $slide['slide_weight'];
        $rating = 0;
        $completion = "0";
        $arcs_a = 0;
        $arcs_r = 0;
        $arcs_c = 0;
        $arcs_s = 0;
        $query = new EntityFieldQuery();
        $query->entityCondition('entity_type', 'seamless_slidedata')
          ->propertyCondition('pid', $pid)
          ->propertyCondition('sid', $sid)
          ->propertyCondition('uid', $uid);
        $result = $query->execute();
        if (isset($result) && is_array($result) && count($result) > 0) {
          $ids = array_keys($result['seamless_slidedata']);
          $entities = entity_load('seamless_slidedata', $ids);
          $slidedata = $entities[$ids[0]];
          $slidedata_wrapper = entity_metadata_wrapper('seamless_slidedata', $slidedata);
          $rating = $slidedata->rating;
          $completion = $slidedata->completion;
          if (isset($slidedata_wrapper->field_arcs_a)) {
            $arcs_a = $slidedata_wrapper->field_arcs_a->value();
          }
          if (isset($slidedata_wrapper->field_arcs_r)) {
            $arcs_r = $slidedata_wrapper->field_arcs_r->value();
          }
          if (isset($slidedata_wrapper->field_arcs_c)) {
            $arcs_c = $slidedata_wrapper->field_arcs_c->value();
          }
          if (isset($slidedata_wrapper->field_arcs_s)) {
            $arcs_s = $slidedata_wrapper->field_arcs_s->value();
          }
        }
        $slide['rating'] = $rating;
        $slide['completion'] = $completion;
        $slide['A'] = $arcs_a;
        $slide['R'] = $arcs_r;
        $slide['C'] = $arcs_c;
        $slide['S'] = $arcs_s;
        $slide['contextId'] = seamless_viewer_get_context_id(array('presentation_id' => $pid, 'slide_id' => $sid));
        $slide['closedQuestions'] = seamless_viewer_get_cq_data_for_slide($sid, $uid, $pid);

        $presentation->slides[] = $slide;
      }
    }
    return $presentation;
  }
  else {
    return services_error(t('Wrong node type.'), 406);
  }
}

/**
 * Webservice callback to retrieve achievement stats.
 *
 * @param int $pid
 *   Presentation id.
 * @param array $data
 *   Data calculated on client side (for future use).
 *
 * @global type $user
 *
 * @return array
 *   Achievement data.
 */
function _ws_seamless_viewer_get_achievement_stats($pid, $data) {
  global $user;
  $user->uid;

  // Get all uids for users with slidedata for this $pid.
  $query = db_select('seamless_slidedata', 'sd')
    ->distinct()
    ->condition('pid', $pid);
  $query->addField('sd', 'uid');
  $uids = array_keys($query->execute()->fetchAllAssoc('uid'));

  $high_score_list = array();

  foreach ($uids as $uid) {
    $presentationdata = seamless_viewer_get_presentation_data($pid, $uid);
    if ($presentationdata === FALSE) {
      // No presentationdata found: initialize.
      $presentationdata = seamless_viewer_update_completion_stats($pid, $uid);
    }
    if (isset($presentationdata->completion_score)) {
      // Uses weights.
      $completed = $presentationdata->completion_score;
    }
    else {
      $completed = 0;
    }
    if (isset($presentationdata->questions_score)) {
      $cq_results = $presentationdata->questions_score;
      $average = round(($completed + $cq_results) / 2, 1);
    }
    else {
      // No questions in presentation.
      $cq_results = NULL;
      $average = round($completed, 1);
    }
    $high_score_list[] = $average;
    if ($uid == $user->uid) {
      $user_completed = $completed;
      $user_cq_results = $cq_results;
      $user_average = $average;
    }
  }
  // Calculate rank.
  sort($high_score_list);
  $high_score_list = array_reverse($high_score_list);
  $n = count($high_score_list);
  $rank = array_search($user_average, $high_score_list);
  $percentile_rank = round((1 - ($rank / $n)) * 100);

  return array(
    'completed' => $user_completed,
    'cq_results' => $user_cq_results,
    'average' => $user_average,
    'percentile_rank' => $percentile_rank,
  );
}

/**
 * Save slide data webservice callback handler.
 *
 * @param int $pid
 *   Presentation node id.
 * @param int $sid
 *   Slide node id.
 * @param array $data
 *   New or updated data.
 *
 * @return array
 *   Returns saved data (or error message).
 */
function _ws_seamless_save_slide_data($pid, $sid, $data) {
  global $user;
  $uid = $user->uid;

  $rating = isset($data['rating']) ? $data['rating'] : NULL;
  $completion = isset($data['completion']) ? $data['completion'] : NULL;
  $arcs_a = isset($data['A']) ? intval($data['A']) : NULL;
  $arcs_r = isset($data['R']) ? intval($data['R']) : NULL;
  $arcs_c = isset($data['C']) ? intval($data['C']) : NULL;
  $arcs_s = isset($data['S']) ? intval($data['S']) : NULL;
  $closedquestion = isset($data['closedquestion']) ? $data['closedquestion'] : NULL;
  // Save slidedata.
  $query = new EntityFieldQuery();
  $query->entityCondition('entity_type', 'seamless_slidedata')
    ->propertyCondition('pid', $pid)
    ->propertyCondition('sid', $sid)
    ->propertyCondition('uid', $uid)
    ->propertyOrderBy('changed', 'DESC')
    ->range(0, 1);
  $result = $query->execute();
  if (isset($result['seamless_slidedata'])) {
    // Existing record, update data.
    $ids = array_keys($result['seamless_slidedata']);
    $entities = entity_load('seamless_slidedata', $ids);
    $slidedata = $entities[$ids[0]];
  }
  else {
    // Create new record.
    $new_record = array(
      'pid' => $pid,
      'sid' => $sid,
      'uid' => $uid,
      'created' => REQUEST_TIME,
    );
    $slidedata = entity_create('seamless_slidedata', $new_record);
  }
  if (!empty($slidedata)) {
    $recalculate_stats = !empty($closedquestion);
    $slidedata_wrapper = entity_metadata_wrapper('seamless_slidedata', $slidedata);
    if ($rating !== NULL) {
      $slidedata->rating = $rating;
    }
    if ($completion !== NULL) {
      if (!isset($slidedata->completion) || $slidedata->completion != $completion) {
        $recalculate_stats = TRUE;
      }
      $slidedata->completion = $completion;
    }
    if ($arcs_a !== NULL) {
      $slidedata_wrapper->field_arcs_a->set($arcs_a);
    }
    if ($arcs_r !== NULL) {
      $slidedata_wrapper->field_arcs_r->set($arcs_r);
    }
    if ($arcs_c !== NULL) {
      $slidedata_wrapper->field_arcs_c->set($arcs_c);
    }
    if ($arcs_s !== NULL) {
      $slidedata_wrapper->field_arcs_s->set($arcs_s);
    }
    $slidedata->changed = REQUEST_TIME;
    $slidedata->save();
    if ($recalculate_stats) {
      // Recalculate completion stats for this presentation and user.
      seamless_viewer_update_completion_stats($pid, $uid);
    }
    return $slidedata;
  }
  return services_error(t('Error saving data.'), 406);
}

/**
 * Return slide data.
 *
 * @param EntityMetadataWrapper $slide_wrapper
 *   Wrapped slide.
 *
 * @return array
 *   Retrieved slide data.
 */
function seamless_viewer_get_slide(EntityMetadataWrapper $slide_wrapper) {
  $panes = array();
  // Initial slide weight experiment:
  // Set initial slide weight=10 so new empty slide is visible on progress bar.
  // Add 60 sec for text or image, add 120 sec for question.
  $sid = $slide_wrapper->getIdentifier();
  $slide_weight = seamless_slides_get_slide_weight($sid);
  $paragraph_fields = array('field_slide_paragraphs', 'field_slide_paragraphs_2');
  foreach ($paragraph_fields as $fld_name) {
    if (isset($slide_wrapper->$fld_name)) {
      $pane = array();
      foreach ($slide_wrapper->$fld_name as $slide_paragraph) {
        $item_bundle = $slide_paragraph->getBundle();
        $item = array();
        $item['type'] = $item_bundle;
        $item['paragraph_id'] = $slide_paragraph->getIdentifier();
        switch ($item_bundle) {
          case 'seamless_question':
            $item['questionId'] = $slide_paragraph->field_slide_question->getIdentifier();
            break;

          default:
            // Return paragraph HTML, rendered by Drupal.
            $para_item = $slide_paragraph->view();
            $para_item['#contextual_links'] = FALSE;
            $para_html = render($para_item);
            $item['html'] = $para_html;
            break;
        }
        $pane[] = $item;
      }
      $panes[$fld_name] = $pane;
    }
  }

  $slide = array(
    'nid' => $sid,
    'title' => $slide_wrapper->label(),
    'template' => $slide_wrapper->field_slide_template->value(),
    'panes' => $panes,
    'slide_weight' => $slide_weight,
  );
  return $slide;
}
