/**
 * Implements seamlessVIewerAPI.
 */

(function (angular) {
  var seamlessViewerAPI = angular.module('seamlessViewerAPI', []);

  /**
   * Webservice API for seamless presentation
   */
  seamlessViewerAPI.factory('seamlessPresentationWS', ['$http', 'serverUrl',
    function seamlessPresentationWS($http, serverUrl) {
      var apiURL = serverUrl + 'seamless_viewer_api/seamless_presentation';
      if (window.Drupal && window.Drupal.settings.seamless_viewer.token) {
        $http.defaults.headers.post['X-CSRF-Token'] = window.Drupal.settings.seamless_viewer.token;
        // add token for delete (no headers.delete so use common ?).
        $http.defaults.headers.common['X-CSRF-Token'] = window.Drupal.settings.seamless_viewer.token;
      }
      return {
        // Get presentation data
        get: function (presentationId, parameters) {
          return $http.get(apiURL + '/' + presentationId + '.json', {cache: false, params: parameters});
        },
        // Save slide data for slide in presentation.
        saveSlideData: function (pid, sid, data) {
          return $http.post(apiURL + '/' + pid + '/saveSlideData/'
            + sid + '.json', data, {cache: false, timeout: 10000});
        },
        // Save slide data for slide in presentation.
        getAchievementStats: function (pid, data) {
          return $http.post(apiURL + '/' + pid + '/getAchievementStats.json', data, {cache: false, timeout: 10000});
        }
      };
    }
  ]);

  /**
   * Webservice API for seamless slide.
   * Used for single slide view mode (not in a presentation).
   */
  seamlessViewerAPI.factory('seamlessSlideWS', ['$http', 'serverUrl',
    function seamlessSlideWS($http, serverUrl) {
      var apiURL = serverUrl + 'seamless_viewer_api/seamless_slide';
      if (window.Drupal && window.Drupal.settings.seamless_viewer.token) {
        $http.defaults.headers.post['X-CSRF-Token'] = window.Drupal.settings.seamless_viewer.token;
        // add token for delete (no headers.delete so use common ?).
        $http.defaults.headers.common['X-CSRF-Token'] = window.Drupal.settings.seamless_viewer.token;
      }
      return {
        get: function (slideId, parameters) {
          return $http.get(apiURL + '/' + slideId + '.json', {cache: false, params: parameters});
        }
      };
    }
  ]);

})(window.angular);
