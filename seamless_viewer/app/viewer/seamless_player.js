(function (angular) {
  var seamlessPlayer = angular.module('seamlessPlayer', ['seamlessViewerAPI']);
  /**
   * Seamless player controller for seamlessViewer.
   * Implements navigation API for seamless presentation viewer.
   */
  seamlessPlayer.controller('seamlessPlayerController', ['$scope', '$routeParams', '$location', '$interval', '$http', '$modal', 'seamlessPresentationWS', 'seamlessSlideWS', 'serverUrl', 'appRoot', 'cachebuster',
    function ($scope, $routeParams, $location, $interval, $http, $modal, seamlessPresentationWS, seamlessSlideWS, serverUrl, appRoot, cachebuster) {
      var self = this;
      if ($location.search().idx) {
        $scope.currentSlideIdx = parseInt($location.search().idx, 10);
      }
      else {
        $scope.currentSlideIdx = 1;
      }
      $scope.slideCount = 0;
      $scope.nid = $routeParams.nid;
      // Preview mode enables display of inline edit options in the viewer.
      $scope.previewMode = ($routeParams.preview == 1);
      $scope.slideEditMode = false;
      $scope.presentation = {};
      // Expose completion info to templates.
      // done = percentage of studyload completed.
      // current = percentage of studyload for current slide.
      // slide_completed = completion status for current slide.
      $scope.completionInfo = {done: 0, current: 0, slide_completed: false};

      this.showMobileCode = false;
      this.showAchievements = false;
      this.mobileCodeSrcRefreshPromise = false;
      this.mobileCodeUrl = '';

      function loadPresentation() {
        var oldCount = $scope.slideCount;
        seamlessPresentationWS.get($scope.nid).then(function (result) {
          $scope.presentation = result.data;
          $scope.slideCount = angular.isArray($scope.presentation.slides) ? $scope.presentation.slides.length : 0;
          if (oldCount > 0 && $scope.slideCount > oldCount) {
            // A slide has been inserted, probably after the currentslide.
            $scope.Next();
          }
          if ($scope.slideCount === 0) {
            self.setSlideAddMode();
          }
          self.UpdateAchievements();
        });
      }
      loadPresentation();

      $scope.currentSlide = function () {
        if ($scope.slideCount > 0) {
          return $scope.presentation.slides[$scope.currentSlideIdx - 1];
        }
      };

      $scope.$watch($scope.currentSlide, function (newSlide, oldSlide) {
        if (newSlide === oldSlide || !newSlide) {
          return;
        }
        else {
          calcProgress();
          $scope.$applyAsync(function () {
            self.UpdateAchievements();
          });
        }
      }, true); // Compare objects in watch.

//      $scope.completionPercentage = function () {
//        var total_weight = $scope.presentation.total_weight;
//        var completed_weight = 0;
//        var percentage = 0;
//        if ($scope.slideCount) {
//          for (var i = 0; i < $scope.slideCount; i++) {
//            var slide = $scope.presentation.slides[i];
//            var weight = parseInt(slide.slide_weight, 10);
//            if (slide.completion == 2) {
//              completed_weight += weight;
//            }
//          }
//          if (total_weight > 0) {
//            percentage = completed_weight / total_weight * 100;
//          }
//        }
//        return percentage;
//      };
//
      function calcProgress() {
        var total_weight = $scope.presentation.total_weight;
        var completed_weight = 0;
        var current_slide_weight = $scope.currentSlide().slide_weight;
        var percentage = 0;
        var current_percentage = 0;
        if ($scope.slideCount) {
          for (var i = 0; i < $scope.slideCount; i++) {
            var slide = $scope.presentation.slides[i];
            var weight = parseInt(slide.slide_weight, 10);
            if (slide.completion == 2) {
              completed_weight += weight;
            }
          }
          if (total_weight > 0) {
            percentage = completed_weight / total_weight * 100;
            current_percentage = current_slide_weight / total_weight * 100;
          }
          $scope.completionInfo.done = percentage;
          $scope.completionInfo.current = current_percentage;
          $scope.completionInfo.slide_completed = false;
          if ($scope.currentSlide().completion == '2') {
            $scope.completionInfo.slide_completed = true;
            $scope.completionInfo.done = percentage - current_percentage;
          }
        }
      }

      $scope.Prev = function () {
        $scope.GotoSlideNr($scope.currentSlideIdx - 1);
      };

      $scope.Next = function () {
        $scope.GotoSlideNr($scope.currentSlideIdx + 1);
      };

      $scope.GotoSlideNr = function (index) {
        $scope.confirmDirtySlide(function () {
          if (index <= $scope.slideCount && index >= 0) {
            $scope.currentSlideIdx = index;
          }
          $location.search('idx', $scope.currentSlideIdx).replace();
        });
      };

      $scope.$on('seamless.slide.deleted', function () {
        self.setSlideViewMode();
      });

      $scope.$on('seamless.slide.updated', function () {
        $scope.addingSlide = false;
        loadPresentation();
      });

      function onLocationChange(event, newstate, oldstate) {
        var idx = $location.search().idx;
        if ($scope.currentSlideIdx < 1) {
          idx = 1;
          $location.search('idx', idx).replace();
        }
        else if ($scope.currentSlideIdx > $scope.slideCount) {
          idx = $scope.slideCount;
          $location.search('idx', idx).replace();
        }
        if (!$scope.slideEditMode) {
          $scope.keepIframe = false; // Close
        }
        $scope.currentSlideIdx = idx;
        angular.element('.seamless-player-main').scrollTop(0);
        $scope.resetDirtySlide(); // Too late, already changed slides!
      }

      $scope.$on('$locationChangeSuccess', onLocationChange);

      this.setSlideEditMode = function () {
        $scope.addingSlide = false;
        $scope.slideEditMode = true;
        $scope.keepIframe = true;
      };

      this.setSlideViewMode = function () {
        $scope.confirmDirtySlide(function () {
          $scope.addingSlide = false;
          $scope.slideEditMode = false;
          $scope.keepIframe = false;
          loadPresentation();
        });
      };

      this.setSlideAddMode = function () {
        $scope.confirmDirtySlide(function () {
          $scope.addingSlide = true;
          $scope.slideEditMode = true;
          $scope.keepIframe = true;
        });
      };

      this.toggleEmulateMobile = function () {
        $scope.emulateMobile = !$scope.emulateMobile;
        if ($scope.emulateMobile) {
          window.frameElement.style.width = '700px';
          window.frameElement.style.height = '350px';
        }
        else {
          window.frameElement.style.width = '100%';
          window.frameElement.style.height = '';
        }
      };

      this.EditInlineSlideUrl = function () {
        if ($scope.addingSlide) {
          return serverUrl + 'seamless_editor_iframe/node/add/slide-text-image?presentationId=' + $scope.nid + '&slideIndex=' + $scope.currentSlideIdx;
        }
        else {
          if ($scope.slideCount > 0) {
            var slide_nid = $scope.presentation.slides[$scope.currentSlideIdx - 1].nid;
            return serverUrl + 'seamless_editor_iframe/node/' + slide_nid + '/edit';
          }
        }
      };

      this.EditSlideUrl = function () {
        if ($scope.slideCount > 0) {
          var slide_nid = $scope.presentation.slides[$scope.currentSlideIdx - 1].nid;
          return serverUrl + 'node/' + slide_nid + '/edit';
        }
      };

      this.EditPresentationUrl = function () {
        return serverUrl + 'node/' + $scope.nid + '/edit';
      };

      this.Exit = function () {
        if (document.referrer) {
          window.location.href = document.referrer;
        }
      };

      this.PrintPresentation = function () {
        window.open(serverUrl + 'seamless_viewer/' + $scope.nid + '/print_view');
      };

      /**
       * Toggles the mobile code.
       *
       * @param {boolean} value
       *   Can be used to force display of the code. By default it toggles.
       */
      this.toggleMobileCode = function (value) {
        $interval.cancel(self.mobileCodeSrcRefreshPromise); // Cancel refresh of QR code.

        if (typeof value === 'undefined') {
          self.showMobileCode = !self.showMobileCode;
        }
        else {
          self.showMobileCode = value;
        }

        // Get listview login URL.
        if (self.showMobileCode) {
          refreshMobileCodeSrc();
          self.mobileCodeSrcRefreshPromise = $interval(refreshMobileCodeSrc, 9 * 60 * 1000); // Refresh every 9 min.
        }
        else {
          refreshMobileCodeSrc(true);
        }
      };

      /**
       * Close MobileCode after click on body.
       */
      angular.element('body').click(function () {
        if (self.showMobileCode) {
          $scope.$applyAsync(function () {
            self.showMobileCode = false;
          });
        }
        if (self.showAchievements) {
          $scope.$applyAsync(function () {
            self.showAchievements = false;
          });
        }
      });

      /**
       * Get a new QR code by refreshing its source.
       *
       * @param {boolean} clear
       *   If true the QR code will be removed.
       */
      function refreshMobileCodeSrc(clear) {
        self.mobileCodeUrlClicked = false;
        if (clear === true) {
          self.mobileCodeSrc = '';
        }
        else {
          var absUrl = serverUrl + 'seamless_viewer/' + $scope.nid + '?idx=' + $scope.currentSlideIdx;

          $http.get(serverUrl + 'seamless/qrcode_url?redirect_url=' + encodeURIComponent(absUrl), {cache: false})
            .then(function (response) {
              self.mobileCodeUrl = response.data.url;
            });
        }
      }

      /**
       * Copies the mobile url to the clipboard.       *
       */
      this.copyMobileCodeToClipboard = function () {
        var dummy = document.createElement('input');
        document.body.appendChild(dummy);
        dummy.setAttribute('value', self.mobileCodeUrl);
        dummy.select();
        document.execCommand('copy');
        document.body.removeChild(dummy);

        // This will let the end user know something has happened.
        self.mobileCodeUrlClicked = true;
      };

      this.userCanUseQRCode = function () {
        return $scope.presentation.allow_qrcode_login;
      };

      /*
       * Try to check for dirty slides.
       */
      function receiveSlideDirtyMessage(e) {
        var data;
        var prefix = '[seamless_slide_dirty]';
        var origin = document.location.origin;

        if (e.data && e.origin && origin === e.origin
          && typeof e.data === 'string' && e.data.indexOf(prefix) === 0) {
          // Parse data.
          data = JSON.parse(e.data.slice(prefix.length));
          var fieldId = data.name + '@' + data.iframe_id;
          $scope.$applyAsync(function () {
            if (data.dirty) {
              addDirtyField(fieldId);
            }
            else {
              removeDirtyField(fieldId);
            }
          });
        }
      }

      function addDirtyField(id) {
        if (!$scope.dirtySlideField) {
          $scope.dirtySlideField = [];
        }
        if ($scope.dirtySlideField.indexOf(id) < 0) {
          $scope.dirtySlideField.push(id);
        }
      }

      function removeDirtyField(id) {
        if (!$scope.dirtySlideField) {
          $scope.dirtySlideField = [];
        }
        var index = $scope.dirtySlideField.indexOf(id);
        if (index >= 0) {
          $scope.dirtySlideField.splice(index, 1);
        }
      }

      $scope.checkDirtySlide = function () {
        if ($scope.dirtySlideField
          && $scope.dirtySlideField.length > 0) {
          return true;
        }
        return false;
      };

      $scope.resetDirtySlide = function () {
        $scope.dirtySlideField = [];
      };

      window.addEventListener('message', receiveSlideDirtyMessage, false);
      $scope.$on('$destroy', function () {
        window.removeEventListener('message', receiveSlideDirtyMessage, false);
      });

      $scope.confirmDirtySlide = function (callbackFunction) {
        // Check for unsaved labjournal notes.
        if ($scope.slideEditMode
          && $scope.dirtySlideField
          && $scope.dirtySlideField.length > 0) {
          var modalInstance = $modal.open({
            backdrop: true,
            templateUrl: appRoot + 'viewer//confirm-modal.html?' + cachebuster,
            controller: ['$scope', '$modalInstance',
              function ($scope, $modalInstance) {
                $scope.message = 'This slide may have unsaved changes!';
                $scope.okCaption = 'Ignore changes';
                $scope.cancelCaption = 'Cancel';
                $scope.ok = function () {
                  $modalInstance.close('ok');
                };
                $scope.cancel = function () {
                  $modalInstance.dismiss('cancel');
                };
              }]
          });

          modalInstance.result.then(function () {
            callbackFunction();
            $scope.resetDirtySlide();
          }, function () {
            // Dismissed.
          });
        }
        else {
          callbackFunction();
        }
      };

      $scope.completed = 0;
      $scope.closedquestionScore = 0;
      $scope.closedquestionCount = 0;
      this.UpdateAchievements = function () {
        var completed = 0;
        var total_weight = $scope.presentation.total_weight;
        var completed_weight = 0;

        var closedquestionScore = 0;
        var closedquestionCount = 0;
        var closedquestionsCorrect = 0;
        var slides = $scope.presentation.slides;
        angular.forEach(slides, function (slide, idx) {
          if (slide.completion == 2) {
            completed_weight += slide.slide_weight;
          }
          if (angular.isArray(slide.closedQuestions)) {
            closedquestionCount += slide.closedQuestions.length;
            angular.forEach(slide.closedQuestions, function (question, idx) {
              if (question.correct) {
                closedquestionsCorrect++;
              }
            });
          }
        }, self);
        if (total_weight > 0) {
          completed = Math.round(1000 * completed_weight / total_weight) / 10;
        }
        $scope.averageScore = completed;
        if (closedquestionCount > 0) {
          closedquestionScore = Math.round(1000 * closedquestionsCorrect / closedquestionCount) / 10;
          $scope.averageScore = (completed + closedquestionScore) / 2;
        }
        $scope.completed = completed;
        $scope.closedquestionScore = closedquestionScore;
        $scope.closedquestionCount = closedquestionCount;
        $scope.achievementsStyle = {'color': 'white'};
        if ($scope.averageScore > 99) {
          $scope.achievementsStyle = {'color': 'gold'};
        }
        else if ($scope.averageScore > 66) {
          $scope.achievementsStyle = {'color': 'silver'};
        }
        else if ($scope.averageScore > 33) {
          $scope.achievementsStyle = {'color': '#CD7F32'}; // Bronze
        }
      };

      $scope.percentileRank = undefined;
      this.ShowAchievements = function () {
        self.UpdateAchievements();
        self.showAchievements = !self.showAchievements;
        if (self.showAchievements) {
          $scope.percentileRank = undefined;
          seamlessPresentationWS.getAchievementStats($scope.nid, {}).then(function (response) {
            var result = response.data;
            $scope.percentileRank = result.percentile_rank;
          });
        }
      };
    }]);

  /**
   * Seamless preview player controller.
   * Displays a player for one single slide.
   * For use in view mode for single seamless-slide node.
   */
  seamlessPlayer.controller('seamlessSlidePreviewController', ['$scope', '$routeParams', 'seamlessSlideWS', 'serverUrl',
    function ($scope, $routeParams, seamlessSlideWS, serverUrl) {
      $scope.nid = $routeParams.nid;
      // Preview mode enables display of inline edit options in the viewer.
      $scope.previewMode = ($routeParams.preview == 1);
      $scope.slide = {};
      seamlessSlideWS.get($scope.nid).then(function (result) {
        $scope.slide = result.data;
      });

      this.EditSlideUrl = function () {
        var slide_nid = $scope.nid;
        return serverUrl + 'node/' + slide_nid + '/edit';
      };

      this.Exit = function () {
        if (document.referrer) {
          window.location.href = document.referrer;
        }
      };
    }]);

  /**
   * Directive for embedding closedquestion iframe.
   * Initializes iframeresizer.
   */
  seamlessPlayer.directive('cqIframe', [
    function cqIframe() {
      return {
        restrict: 'A',
        link: function (scope, element, attrs) {
          element.iFrameResize({checkOrigin: false});
          scope.$on('$destroy', function () {
            element[0].iFrameResizer.close();
          });
        }
      };
    }]);

  /**
   * Seamless preview player controller.
   * Displays a player for one single slide.
   * For use in view mode for single seamless-slide node.
   */
  seamlessPlayer.controller('seamlessPrintController', ['$scope', '$routeParams', 'serverUrl', 'seamlessPresentationWS',
    function seamlessPrintController($scope, $routeParams, serverUrl, seamlessPresentationWS) {
      $scope.nid = $routeParams.nid;
      $scope.presentation = {};
      // Expose completion info to templates.
      // done = percentage of studyload completed.
      // current = percentage of studyload for current slide.
      // slide_completed = completion status for current slide.
      $scope.completionInfo = {done: 0, current: 0, slide_completed: false};

      function loadPresentation() {
        seamlessPresentationWS.get($scope.nid).then(function (result) {
          $scope.presentation = result.data;
        });
      }
      loadPresentation();

//      $scope.$on('$viewContentLoaded', function () {
//        $scope.$evalAsync (
//          function () {
//            window.print();
//          }
//        );
//        //window.close();
//      });

    }]);

  /**
   * Seamless slide printing directive.
   * Display the slide, passed in the slide attribute.
   * Also responsible for updating changes in completion or rating for the slide.
   */
  seamlessPlayer.directive('seamlessPrintSlide', ['appRoot', 'cachebuster', 'serverUrl', 'seamlessPresentationWS',
    function seamlessPrintSlide(appRoot, cachebuster, serverUrl, seamlessPresentationWS) {
      return {
        restrict: 'E',
        templateUrl: appRoot + 'viewer/seamless-print-slide.html?' + cachebuster,
        scope: {
          slide: '=slide'
        },
        link: function (scope, element, attrs) {
          var pid = scope.$parent.nid;
          var slideIdx = scope.$parent.$index + 1;
          scope.slideUrl = serverUrl + 'seamless_viewer/' + pid + '?idx=' + slideIdx;
        }
      };
    }]);

  /**
   * Seamless slide directive.
   * Display the slide, passed in the slide attribute.
   * Also responsible for updating changes in completion or rating for the slide.
   */
  seamlessPlayer.directive('seamlessSlide', ['appRoot', 'cachebuster', 'serverUrl', 'seamlessPresentationWS',
    function seamlessSlide(appRoot, cachebuster, serverUrl, seamlessPresentationWS) {
      return {
        restrict: 'E',
        templateUrl: appRoot + 'viewer/seamless-slide.html?' + cachebuster,
        scope: {
          slide: '=slide'
        },
        link: function (scope, element, attrs) {

          scope.saveRating = function () {
            var pid = scope.$parent.nid;
            var sid = scope.slide.nid;
            var data = {
              rating: scope.slide.rating
            };
            seamlessPresentationWS.saveSlideData(pid, sid, data);
          };

          scope.saveARCS = function () {
            var pid = scope.$parent.nid;
            var sid = scope.slide.nid;
            var data = {
              A: scope.slide.A,
              R: scope.slide.R,
              C: scope.slide.C,
              S: scope.slide.S
            };
            seamlessPresentationWS.saveSlideData(pid, sid, data);
          };

          scope.saveCompletion = function () {
            var pid = scope.$parent.nid;
            var sid = scope.slide.nid;
            var data = {
              completion: scope.slide.completion
            };
            seamlessPresentationWS.saveSlideData(pid, sid, data);
          };

          /**
           * Callback function from cq-form directive.
           * Update slide closedQuestion data when question is answered.
           *
           * @param {type} data
           * @return {undefined}
           */
          scope.updateQuestionData = function (data) {
            var context_id = data.contextId;
            var qid = data.nid;
            var feedback = data.feedback;
            var correct = data.correct;
            var answer = data.answer;
            var answered = (answer !== null);

            var questions = scope.slide.closedQuestions;
            if (questions && angular.isArray(questions)) {
              for (var i = 0; i < questions.length; i++) {
                if (questions[i].context_id === context_id && questions[i].qid == qid) {
                  if (questions[i].answered !== answered
                    || questions[i].correct !== correct) {
                    // Only update when completion state changed.
                    // Posting of closedquestion data triggers recalculation
                    // of completion for this presentation.
                    questions[i].answered = answered;
                    questions[i].correct = correct;
                    questions[i].feedback = feedback;
                    questions[i].answer = answer;

                    var pid = scope.$parent.nid;
                    var sid = scope.slide.nid;
                    var update_data = {
                      closedquestion: questions[i]
                    };
                    seamlessPresentationWS.saveSlideData(pid, sid, update_data);
                  }
                }
              }
            }
          };


          scope.$watch('slide', function (newSlide, oldSlide) {
            // Initializing (both undefined).
            if (newSlide === oldSlide || !newSlide) {
              return;
            }

            if (newSlide !== oldSlide) {
              if (newSlide.completion === '0') {
                // First time, set completion to 1.
                newSlide.completion = '1';
              }
              else {
                if (angular.isObject(oldSlide) && newSlide.nid === oldSlide.nid) {
                  if (newSlide.rating !== oldSlide.rating) {
                    scope.saveRating();
                  }
                  if (newSlide.A !== oldSlide.A || newSlide.R !== oldSlide.R || newSlide.C !== oldSlide.C || newSlide.S !== oldSlide.S) {
                    scope.saveARCS();
                  }
                  if (newSlide.completion !== oldSlide.completion) {
                    scope.saveCompletion();
                  }
                }
              }
            }
          }, true);
        }
      };
    }]);

  seamlessPlayer.directive('seamlessPlayerHeader', ['appRoot', 'cachebuster',
    function seamlessPlayerHeader(appRoot, cachebuster) {
      return {
        restrict: 'EC',
        templateUrl: appRoot + 'viewer/seamless-header.html?' + cachebuster,
        scope: false, // Use parent scope.
        link: function (scope, element, attrs) {

        }
      };
    }]);

  seamlessPlayer.directive('seamlessPlayerFooter', ['appRoot', 'cachebuster',
    function seamlessPlayerFooter(appRoot, cachebuster) {
      return {
        restrict: 'EC',
        templateUrl: appRoot + 'viewer/seamless-footer.html?' + cachebuster,
        scope: false, // Use parent scope.
        link: function (scope, element, attrs) {

        }
      };
    }]);

  seamlessPlayer.directive('seamlessProgressbar', ['appRoot', 'cachebuster',
    function seamlessProgressbar(appRoot, cachebuster) {
      return {
        restrict: 'E',
        templateUrl: appRoot + 'viewer/seamless-progressbar.html?' + cachebuster,
        scope: false, // Use parent scope.
        link: function (scope, element, attrs) {

        }
      };
    }]);

  seamlessPlayer.directive('seamlessArcsPanel', ['appRoot', 'cachebuster',
    function seamlessArcsPanel(appRoot, cachebuster) {
      return {
        restrict: 'E',
        templateUrl: appRoot + 'viewer/seamless-arcs-panel.html?' + cachebuster,
        scope: false, // Use parent scope.
        link: function (scope, element, attrs) {

        }
      };
    }]);

  seamlessPlayer.directive('seamlessAchievementsPanel', ['appRoot', 'cachebuster',
    function seamlessPlayerHeader(appRoot, cachebuster) {
      return {
        restrict: 'EC',
        templateUrl: appRoot + 'viewer/seamless-achievements-panel.html?' + cachebuster,
        scope: false, // Use parent scope.
        link: function (scope, element, attrs) {

        }
      };
    }]);

  /**
   * Need this directive to compile the directives in the html
   *
   */
  seamlessPlayer.directive('compileHtml', ['$compile',
    function compile($compile) {
      return {
        restrict: 'A',
        link: function (scope, element, attrs) {
          scope.$watch(attrs.compileHtml,
            function (value) {
              element.html(value);
              $compile(element.contents())(scope);
            }
          );
        }
      };
    }]);

  /**
   * Emulate functionality of collapse_text module in angular directive.
   */
  seamlessPlayer.directive('collapseTextFieldset', [
    function collapseTextFieldset() {
      return {
        restrict: 'C',
        scope: true,
        link: function (scope, element, attrs) {
          element.find('> legend span.fieldset-legend').click(function () {
            if (element.hasClass('collapsed')) {
              element.removeClass('collapsed');
            }
            else {
              element.addClass('collapsed');
            }
          });
          // Uncollapse collapsibles in print template.
          if (element.hasClass('collapsed') && element.closest('.seamless-template-print').length > 0) {
            element.removeClass('collapsed');
          }
        }
      };
    }]);

  seamlessPlayer.directive('seamlessQrcodePanel', ['appRoot', 'cachebuster',
    function seamlessPlayerHeader(appRoot, cachebuster) {
      return {
        restrict: 'EC',
        templateUrl: appRoot + 'viewer/seamless-qrcode.html?' + cachebuster,
        scope: false, // Use parent scope.
        link: function (scope, element, attrs) {

        }
      };
    }]);

  /**
   * Turns <qrcode> tags into qr codes using https://github.com/kazuhikoarase/qrcode-generator
   */
  seamlessPlayer.directive('qrcode', [function () {
      return {
        restrict: 'E',
        replace: true,
        scope: {
          "targetUrl": "@"
        },
        link: function (scope, element, attrs, ctrl) {
          function setCode(URL) {
            var typeNumber = (URL.length < 96 ? 4 : 8); // Higher numbers allow for more data
            var errorCorrectionLevel = 'L';
            var cellSize = 3;
            var margin = 4;
            var qr = qrcode(typeNumber, errorCorrectionLevel);

            qr.addData(URL);
            qr.make();
            element.html(qr.createImgTag(cellSize, margin)).attr('title', URL);
          }

          scope.$watch('targetUrl', function (newValue, oldValue) {
            if (newValue) {
              setCode(newValue);
            }
          }, true);
          if (scope.targetUrl) {
            setCode(scope.targetUrl);
          }
        }
      };
    }]);

  /**
   * Filter to allow potentially unsafe html.
   * e.g. allow youtube player iframe etc.
   */
  seamlessPlayer.filter('trusted', ['$sce', function ($sce) {
      return function (text) {
        return $sce.trustAsHtml(text);
      };
    }]);
})(window.angular);


