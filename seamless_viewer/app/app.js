(function () {
  // Load angular app after Drupal has initialized the Drupal.settings  property
  Drupal.behaviors.seamlessViewerModule = {
    attach: function (context, settings) {
      var moduleList = Drupal.settings.seamless_viewer.angularModuleList;
      var seamlessViewer = angular.module('seamlessViewer', moduleList);
      seamlessViewer
        .config(['$locationProvider',
          function ($locationProvider) {
            $locationProvider.html5Mode(true);
          }])
        .config(['$provide',
          function ($provide) {
            $provide.constant('appRoot', Drupal.settings.basePath + Drupal.settings.seamless_viewer.modulePath + '/app/');
            var absoluteBaseUrl = Drupal.absoluteUrl(Drupal.settings.basePath);
            $provide.constant('serverUrl', absoluteBaseUrl);
            $provide.constant('cachebuster', Drupal.settings.seamless_viewer.cachebuster);
          }])
        .config(['$routeProvider', 'appRoot', 'cachebuster',
          function ($routeProvider, appRoot, cachebuster) {
            $routeProvider.
              when('/', {
                templateUrl: appRoot + 'viewer/seamless-viewer.html?' + cachebuster,
//                controller: 'seamlessViewerController',
//                controllerAs: 'seamlessCtrl',
                reloadOnSearch: false
              }).
              when('/:nid', {
                templateUrl: appRoot + 'viewer/seamless-viewer.html?' + cachebuster,
                controller: 'seamlessPlayerController',
                controllerAs: 'seamlessPlayerCtrl',
                reloadOnSearch: false
              }).
              when('/:nid/print_view', {
                templateUrl: appRoot + 'viewer/seamless-print-viewer.html?' + cachebuster,
                controller: 'seamlessPrintController',
                controllerAs: 'seamlessPrintCtrl',
                reloadOnSearch: false
              }).
              when('/:nid/slide_preview', {
                templateUrl: appRoot + 'viewer/seamless-preview.html?' + cachebuster,
                controller: 'seamlessSlidePreviewController',
                controllerAs: 'seamlessSlidePreviewCtrl',
                reloadOnSearch: false
              }).
              otherwise({
                redirectTo: '/'
              });
          }
        ]);
    }
  };
})();
