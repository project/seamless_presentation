<?php

/**
 * @file
 * seamless_viewer.services.inc
 */

/**
 * Implements hook_default_services_endpoint().
 */
function seamless_viewer_default_services_endpoint() {
  $export = array();

  $endpoint = new stdClass();
  $endpoint->disabled = FALSE; /* Edit this to true to make a default endpoint disabled initially */
  $endpoint->api_version = 3;
  $endpoint->name = 'seamless_viewer_api';
  $endpoint->server = 'rest_server';
  $endpoint->path = 'seamless_viewer_api';
  $endpoint->authentication = array(
    'services' => 'services',
  );
  $endpoint->server_settings = array(
    'formatters' => array(
      'json' => TRUE,
      'bencode' => FALSE,
      'jsonp' => FALSE,
      'php' => FALSE,
      'xml' => FALSE,
      'yaml' => FALSE,
    ),
    'parsers' => array(
      'application/json' => TRUE,
      'application/x-www-form-urlencoded' => FALSE,
      'application/x-yaml' => FALSE,
      'application/xml' => FALSE,
      'multipart/form-data' => FALSE,
      'text/xml' => FALSE,
    ),
  );
  $endpoint->resources = array(
    'seamless_presentation' => array(
      'operations' => array(
        'retrieve' => array(
          'enabled' => '1',
        ),
        'index' => array(
          'enabled' => '1',
        ),
      ),
      'targeted_actions' => array(
        'saveSlideData' => array(
          'enabled' => '1',
        ),
        'getAchievementStats' => array(
          'enabled' => '1',
        ),
      ),
    ),
    'seamless_slide' => array(
      'operations' => array(
        'retrieve' => array(
          'enabled' => '1',
        ),
      ),
    ),
  );
  $endpoint->debug = 0;
  $export['seamless_viewer_api'] = $endpoint;

  return $export;
}
