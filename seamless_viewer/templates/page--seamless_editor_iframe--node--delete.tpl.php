<!-- START page--seamless_editor_iframe--node--edit.tpl -->
<div id="page">
  <div id="content" class="clearfix">
    <?php if ($messages): ?>
      <div id="console" class="clearfix"><?php print $messages; ?></div>
    <?php endif; ?>
    <div id="content-wrapper">
      <div id="main-content">
        <?php print render($page['content']); ?>
      </div>
    </div>
  </div>
</div>
<!-- END page--seamless_editor_iframe--node--edit.tpl -->
