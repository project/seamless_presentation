<?php

// We hide the comments and links and only render plain content.
hide($content['comments']);
hide($content['links']);
print render($content);
?>
