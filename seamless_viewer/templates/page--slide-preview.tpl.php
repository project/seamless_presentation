<!-- START page--slide-preview.tpl -->
<div id="page">
  <!-- Move tabs to header like to match Adminimal theme -->
  <header role="banner" class="l-header">

    <!-- Move tabs to header like to match Adminimal theme -->
    <?php if (!empty($tabs)): ?>
      <?php print render($tabs); ?>
      <?php
      if (!empty($tabs2)): print render($tabs2);
      endif;
      ?>
    <?php endif; ?>
  </header>
  <div id="seamless-content">
    <?php print render($page['content']); ?>
  </div>
</div>
<!-- END page--slide-preview.tpl -->
