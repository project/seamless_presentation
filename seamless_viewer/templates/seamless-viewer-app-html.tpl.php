<!DOCTYPE html>
<html>

  <head>
    <title><?php print $title; ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <base href="<?php print base_path() . 'seamless_viewer/'; ?>">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <?php print $styles; ?>
    <?php print $scripts; ?>
  </head>
  <body class="seamless-body">
    <?php print $content; ?>
  </body>
</html>
