(function ($) {

  function htmlToText(html) {
    var dom = document.createElement('DIV');
    dom.innerHTML = html;
    var plain_text = (dom.textContent || dom.innerText);
    return plain_text;
  }

  function showTextStatistics(stats) {
    var textStatistics = '';
    if (stats) {
      // textStatistics += '<div style="width: 100%; color: gray;"><strong>Detailed text analysis results.</strong></div>';
      textStatistics += '<div><strong>Text statistics</strong>';
      textStatistics += '<br>text length: ' + stats.textLength();
      textStatistics += '<br>letter count: ' + stats.letterCount();
      textStatistics += '<br>sentence count: ' + stats.sentenceCount();
      textStatistics += '<br>word count: ' + stats.wordCount();
      textStatistics += '<br>average words per sentence: ' + stats.averageWordsPerSentence();
      textStatistics += '<br>average syllables per word: ' + stats.averageSyllablesPerWord();
      textStatistics += '</div>';

      textStatistics += '<div><strong>Readbility scores</strong>';
      textStatistics += '<br>Flesch-Kincaid Reading Ease: ' + stats.fleschKincaidReadingEase();
      textStatistics += '<br>Flesch-Kincaid Grade Level: ' + stats.fleschKincaidGradeLevel();
      textStatistics += '<br>Gunning-Fog Score: ' + stats.gunningFogScore();
      textStatistics += '<br>Coleman-Liau Index: ' + stats.colemanLiauIndex();
      textStatistics += '<br>Smog Index: ' + stats.smogIndex();
      textStatistics += '<br>Automated Readability Index: ' + stats.automatedReadabilityIndex();
      textStatistics += '</div>';
    }
    return textStatistics;
  }

  /**
   * Creates a traffic light based on readability values and adds it to a container.
   * Note: requires Drupal.t function.
   *
   * @param {object} $parentDOMContainer
   *   Reference to parent DOM elemen to insert trafficlight.
   * @param {object} textStats
   *   A textstatistics object.

   **/
  function readabilityTrafficLight($parentDOMContainer, textStats) {
    var className;
    var $trafficLightTemplate = $('<div class="readibilityTrafficLightContainer"><div class="readibilityTrafficLight"><div class="readibilityTrafficLightRed readibilityTrafficLightBulb"></div><div class="readibilityTrafficLightOrange readibilityTrafficLightBulb"></div><div class="readibilityTrafficLightGreen readibilityTrafficLightBulb"></div></div><div class="readibilityTrafficLightFeedback"></div></div>');
    var readabilityResults = {
      fleschKincaidGradeLevel: textStats.fleschKincaidGradeLevel(),
      gunningFogScore: textStats.gunningFogScore(),
      colemanLiauIndex: textStats.colemanLiauIndex(),
      smogIndex: textStats.smogIndex(),
      automatedReadabilityIndex: textStats.automatedReadabilityIndex()
    };

    var readabilityConfig = {
      'fleschKincaidGradeLevel': {
        '1': [0, 9],
        '2': [10, 14],
        '3': [15, 99]
      },
      'gunningFogScore': {
        '1': [0, 9],
        '2': [10, 14],
        '3': [15, 99]
      },
      'colemanLiauIndex': {
        '1': [0, 9],
        '2': [10, 14],
        '3': [15, 99]
      },
      'smogIndex': {
        '1': [0, 10],
        '2': [11, 15],
        '3': [16, 99]
      },
      'automatedReadabilityIndex': {
        '1': [0, 15],
        '2': [16, 18],
        '3': [19, 99]
      }
    };

    /* Process results. */
    var testResult = 0;
    var numberOfTests = 0;
    $.each(readabilityConfig, function (testName, testLevels) {
      numberOfTests++;
      var sampleTestValue = readabilityResults[testName];

      $.each(testLevels, function (points, boundaries) {
        if (sampleTestValue >= boundaries[0] && sampleTestValue <= boundaries[1]) {
          testResult += parseInt(points, 10);
        }
      });
    });

    /* Calculate average and determine feedback. */
    var feedback = '';
    testResult = Math.round(testResult / numberOfTests);
    switch (testResult) {
      case 1:
        className = '.readibilityTrafficLightGreen';
        feedback = Drupal.t('This text is easy to understand.');
        break;
      case 2:
        className = '.readibilityTrafficLightOrange';
        feedback = Drupal.t('This text is not very easy to understand. Try to use shorter words and shorter sentences.');
        break;
      case 3:
        className = '.readibilityTrafficLightRed';
        feedback = Drupal.t('This text is difficult to understand. Try to use shorter words and shorter sentences.');
        break;
    }

    /* Add traffic light to DOM. */
    $parentDOMContainer.prepend($trafficLightTemplate);
    $trafficLightTemplate.find(className).addClass('readibilityTrafficLightActive');
    $trafficLightTemplate.find('.readibilityTrafficLightFeedback').html(feedback);

  }

  Drupal.behaviors.seamless_slides = {
    attach: function (context, settings) {
      var origin = document.location.origin;

      /**
       * Updates the dirty status of an elemente in the parent of this iframe.
       * @param {string} elementId
       *   Id of the element.
       * @param {boolean} isDirty
       *   Dirty flag for elemen.
       */
      function updateElementDirtyStatus(elementId, isDirty) {
        var frame_data = {
          iframe_id: window.frameElement.id,
          dirty: isDirty,
          name: elementId
        };

        parent.postMessage('[seamless_slide_dirty]' + JSON.stringify(frame_data), origin);
      }

      // Wire up CKEditor.
      if (typeof CKEDITOR !== 'undefined' && CKEDITOR.seamlessHooksInstalled !== true) {
        // Install hooks only once.
        CKEDITOR.seamlessHooksInstalled = true;
        CKEDITOR.on('instanceReady', function (evt) {

          function updateStats() {
            // getData() returns CKEditor's HTML content.
            var htmlData = instance.getData();
            if (htmlData) {
              var textStats = new textstatistics(htmlData);
              if (showDetails) {
                statsDisplay.html(showTextStatistics(textStats));
              }
              else {
                statsDisplay.html('');
              }
              readabilityTrafficLight(statsDisplay, textStats);
            }
            else {
              statsDisplay.html('');
            }
          }

          var instance = evt.editor;
          var id = instance.name;
          // Get CKEditor container id.
          var field_id = instance.container.getId();
          // Clear dirty flag on load.
          var isDirty = false;
          updateElementDirtyStatus(field_id, false);
          var showDetails = false;
          // Insert toggle to display text statistics.
          $('<label><input type="checkbox" id="text-statistics-' + id + '-toggle"> Show detailed text statistics</label>')
            .addClass('seamless-text-statistics-toggle')
            .insertAfter('#cke_' + id)
            .bind('change', function (e) {
              var value = e.target.checked;
              showDetails = value;
              updateStats();
            }).hide();
          // Insert div to display text statistics.
          var statsDisplay = $('<div id="text-statistics-' + id + '"></div>')
            .addClass('seamless-text-statistics')
            .insertAfter('#cke_' + id);
          updateStats();
//          instance.on('blur', function (evt) { // Update on blur event.
          instance.on('change', function (e) { // Update on change event(version 4.2 or higher of ckeditor).
            if (statsDisplay.is(':visible')) { // Only when stats display is visible.
              updateStats();
            }
            if (!isDirty) { // Only trigger on first change.
              isDirty = true;
              updateElementDirtyStatus(field_id, true);
            }
          });
        });
      }

      if (context == document) { // Only on initial load, not on ajax reloads.

        /* Textareas. */

        // Reset dirty flag on load.
        $('textarea.form-textarea', context).each(function () {
          updateElementDirtyStatus($(this).attr('id'), false);
        });

        // Set dirty flag on keyup/paste.
        $('textarea.form-textarea', context).on('keyup, paste', function () {
          updateElementDirtyStatus($(this).attr('id'), true);
        });

        /* Text fields. */

        // Reset dirty flag on load.
        $('input[type=text].form-text', context).each(function () {
          updateElementDirtyStatus($(this).attr('id'), false);
        });

        // Set dirty flag on keyup/paste.
        $('input[type=text].form-text', context).on('keyup paste', function () {
          updateElementDirtyStatus($(this).attr('id'), true);
        });

        /* Radios. */

        // Reset dirty flag on load.
        $('input.form-radio', context).each(function () {
          updateElementDirtyStatus($(this).attr('id'), false);
        });

        // Set dirty flag on change.
        $('input.form-radio', context).on('change', function () {
          updateElementDirtyStatus($(this).attr('id'), true);
        });

        /* Checkboxes. */

        // Reset dirty flag on load.
        $('input.form-checkbox', context).each(function () {
          updateElementDirtyStatus($(this).attr('id'), false);
        });

        // Set dirty flag on change.
        $('input.form-checkbox', context).on('change', function () {
          updateElementDirtyStatus($(this).attr('id'), true);
        });

        // Reset dirty flag on load.
        $('select', context).each(function () {
          updateElementDirtyStatus($(this).attr('id'), false);
        });

        // Set dirty flag on change.
        $('select', context).on('change', function () {
          updateElementDirtyStatus($(this).attr('id'), true);
        });

        /* Ajax submit buttons. */

        // Reset dirty flag on load.
        $('input[type=submit].form-submit.ajax-processed', context).each(function () {
          updateElementDirtyStatus($(this).attr('id'), false);
        });

        // Set dirty flag on click (ajax buttons use mousedown).
        $('input[type=submit].form-submit.ajax-processed', context).on('mousedown', function () {
          updateElementDirtyStatus($(this).attr('id'), true);
        });
      }
    }
  };

})(jQuery);
