<?php

/**
 * @file
 * seamless_slides.features.field_base.inc
 */

/**
 * Implements hook_field_default_field_bases().
 */
function seamless_slides_field_default_field_bases() {
  $field_bases = array();

  // Exported field_base: 'field_arcs_attention'.
  $field_bases['field_arcs_attention'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_arcs_attention',
    'indexes' => array(
      'value' => array(
        0 => 'value',
      ),
    ),
    'locked' => 0,
    'module' => 'list',
    'settings' => array(
      'allowed_values' => array(
        0 => 'No elements',
        1 => 'Some elements',
        2 => 'Many elements',
      ),
      'allowed_values_function' => '',
    ),
    'translatable' => 0,
    'type' => 'list_integer',
  );

  // Exported field_base: 'field_arcs_confidence'.
  $field_bases['field_arcs_confidence'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_arcs_confidence',
    'indexes' => array(
      'value' => array(
        0 => 'value',
      ),
    ),
    'locked' => 0,
    'module' => 'list',
    'settings' => array(
      'allowed_values' => array(
        0 => 'No',
        1 => 'Somewhat',
        2 => 'Yes',
      ),
      'allowed_values_function' => '',
    ),
    'translatable' => 0,
    'type' => 'list_integer',
  );

  // Exported field_base: 'field_arcs_explanation'.
  $field_bases['field_arcs_explanation'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_arcs_explanation',
    'indexes' => array(),
    'locked' => 0,
    'module' => 'markup',
    'settings' => array(
      'markup' => array(
        'format' => 'full_html',
        'value' => '<h2>Reflect on each slide as follows:</h2><p>Do I expect that this slide helps to:</p><ul style="margin-left: 40px;"><li>A : capture the attention of my students?</li><li>R : make my students aware of the relevance of what it presents?</li><li>C : prevent undermining the confidence of my students?</li><li>S : provide my students with satisfying learning experiences?</li></ul><p>&nbsp;</p><h2>Note: the ARCS variables are not mutually independent</h2><p>For instance:</p><ul style="margin-left: 40px;"><li>Raising awareness of relevance helps to capture attention (even though capturing attention does not require awareness of relevance).</li><li>When students pay little attention awareness of relevance is likely to be low.</li><li>Often, satisfaction comes after successful completion of a challenging task but a challenging task requires attention.</li><li>Successful completion of a challenging task can be very satisfying, but a challenge encountered too early may undermine confidence.</li><li>Working on a challenging task that seems irrelevant may not result in satisfaction.</li><li>Without confidence it is difficult to stay interested and focus on a challenging task.</li></ul><p>&nbsp;</p><h2>Reflect on each slide but: aim to realize satisfactory ARCS values at <u><em>section level</em></u></h2><ul style="margin-left: 40px;"><li>We advise to reflect on every slide that you composed and to check<ul><li>Does this slide contribute to A, to R, to S, and what does this slide to C?</li></ul></li><li>Ideally, each seamless slide will contribute to each ARCS variable</li><li>However, in practice it is seldom possible to attain this ideal</li><li>t is more realistic to ensure that at least in each section there are sufficient contributions to each ARCS variable</li></ul>',
      ),
    ),
    'translatable' => 0,
    'type' => 'markup',
  );

  // Exported field_base: 'field_arcs_relevance'.
  $field_bases['field_arcs_relevance'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_arcs_relevance',
    'indexes' => array(
      'value' => array(
        0 => 'value',
      ),
    ),
    'locked' => 0,
    'module' => 'list',
    'settings' => array(
      'allowed_values' => array(
        0 => 'No',
        1 => 'Somewhat',
        2 => 'Yes',
      ),
      'allowed_values_function' => '',
    ),
    'translatable' => 0,
    'type' => 'list_integer',
  );

  // Exported field_base: 'field_arcs_satisfaction'.
  $field_bases['field_arcs_satisfaction'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_arcs_satisfaction',
    'indexes' => array(
      'value' => array(
        0 => 'value',
      ),
    ),
    'locked' => 0,
    'module' => 'list',
    'settings' => array(
      'allowed_values' => array(
        0 => 'No',
        1 => 'Somewhat',
        2 => 'Yes',
      ),
      'allowed_values_function' => '',
    ),
    'translatable' => 0,
    'type' => 'list_integer',
  );

  // Exported field_base: 'field_arcs_tips_attention'.
  $field_bases['field_arcs_tips_attention'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_arcs_tips_attention',
    'indexes' => array(),
    'locked' => 0,
    'module' => 'markup',
    'settings' => array(
      'markup' => array(
        'format' => 'full_html',
        'value' => '<ul style="margin-left: 40px;"><li>Assume that your audience will at least have some interest in the scientific, technological, social field(s) in to which your Seamless eBook belongs.</li><li>Critically reflect on any other assumption about your target audience:</li><li>if possible try to find out more e.g. about their prior knowledge, ...</li><li>Stimulate interest with surprising stories, data and experiments<ul><li>(In order to surprise you must guess what is their prior knowledge....)</li></ul></li><li>Start a topic with intriguing questions</li><li>Present paradoxes, conflicting interpretations of data, data from different sources that seem to be conflicting, ...</li><li>In connection to the topic of the section that you write: incorporate short discussions of well known fallacies/misunderstandings that are related to that topic<ul><li>E.g. When introducing Newton’s third law<br>discuss how people tend to forget that the action force and the reaction force do not act on the same body.</li><li>E.g. When discussing relationships or associations between data sets<br>discuss correlations that do not make sense</li></ul></li><li>Incorporate questions of which students most likely will feel that they should be able to answer them<ul><li>E.g. In almost any science course:<br>estimate the number of molecules in a drop of water, estimate the number of molecules in a gram of salt</li></ul></li></ul>',
      ),
    ),
    'translatable' => 0,
    'type' => 'markup',
  );

  // Exported field_base: 'field_arcs_tips_confidence'.
  $field_bases['field_arcs_tips_confidence'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_arcs_tips_confidence',
    'indexes' => array(),
    'locked' => 0,
    'module' => 'markup',
    'settings' => array(
      'markup' => array(
        'format' => 'full_html',
        'value' => '<p>Avoid unnecessary cognitive load</p><ul style="margin-left: 40px;"><li>Avoid passive voice.</li><li>Avoid long sentences.</li><li>Avoid any abreviation or symbol <em><u>unless </u></em>it is used very often.</li><li>Avoid archaic language.</li><li>Avoid words that are seldom used in natural language <em><u>unless&nbsp;</u></em>&nbsp;these words are part of the standard terminology in your field.</li><li>Always provide examples and explanations of any term that is not basic natural language.<br>Be aware that basic natural language often is ambiguous.</li><li>Always provide legend with diagrams and graphs</li></ul><ul style="margin-left: 40px;"><li>Introduce any concept, method, law, ... as follows:<ul><li><u><em><strong>first </strong></em></u>give some examples</li><li><u><em><strong>then </strong></em></u>formulate and explain the concept,&nbsp; ... that covers these examples</li></ul></li><li>Many concepts, .. in scientific fields were the result of work by many scientists over periods of many decades or even ages.<br>Make this clear to your students.</li><li>Even when a certain concept, .. is attributed to one scientist this scientist will have spent many hours developing it.<br>Make this clear to your students.</li><li>If a student needs to invest considerable effort in ‘grasping’ the concept, .. and learning to use it, (s)he is not the only one.<br>Make this clear to your students.</li></ul><ul style="margin-left: 40px;"><li>Try to compose slides such that each slide more or less will require the same amount of effort.</li><li>When a specific slide is likely to require more effort than other slides:<br>warn the student to allocate more time to studying that slide.<br>In other words:<br>let your students know that it is the content of the slide that ‘slows them down’ rather than their own inabilities.</li><li>Provide ‘scaffolding’ along with any ‘authentic’ assignments<br>(i.e. to do tasks of a professional)</li><li>Gradually decrease ‘scaffolding’ in successive assignments</li><li>Present the more challenging assignments and questions without any scaffolding near the end of a topic.</li><li>When the student needs to learn many details provide many assignments that require him/her to use these details</li></ul>',
      ),
    ),
    'translatable' => 0,
    'type' => 'markup',
  );

  // Exported field_base: 'field_arcs_tips_relevance'.
  $field_bases['field_arcs_tips_relevance'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_arcs_tips_relevance',
    'indexes' => array(),
    'locked' => 0,
    'module' => 'markup',
    'settings' => array(
      'markup' => array(
        'format' => 'full_html',
        'value' => '<ul style="margin-left: 40px;"><li>Connect to recent news within these fields<ul><li>New inventions, new product developments, new discoveries, new trends, new interests, recent food scandals, recent outbreaks, recent disasters ...</li><li>Make explicit how the concepts, laws, formula’s that you introduce and explain have been used or should have been used in such events and processes.</li></ul></li><li>Make explicit why mastering the information and challenges in the section is essential in order to become a professional in the fields at which the curriculum aims.<ul><li>E.g. In a food technology course, make explicit why every professional in the field of food technology should understand the Monod equation</li></ul></li><li>Whenever slides introduce fundamental concepts, laws, formula’s, ... make clear that professionals in the field use these concepts, ... all the time.<br>(Even though such use will not always be visible or obvious to any laymen)<br>Thus it pays off if they invest a lot of effort in order to grasp the concept, law, formula...</li><li>Connect to well-known mistakes/errors made by professionals in the field and link to well-known misconceptions of many students</li><li>Connect to famous successes made by professionals in the field</li><li>At the level of the eBOOK:<ul><li>Make explicit how the intended learning outcomes of the course are related to the work of professionals in the field</li><li>Make explicit how the intended learning outcomes of the course will be assessed (in an exam or otherwise)</li><li>In other words guarantee ‘constructive alignment’ and make this explicit</li></ul></li><li>At the level of chapters, sections and possibly at the slide level<ul><li>Make explicit how slides and sections are related to intended learning outcomes and how they will come back in assessments (e.g. in the exam)</li></ul></li></ul>',
      ),
    ),
    'translatable' => 0,
    'type' => 'markup',
  );

  // Exported field_base: 'field_arcs_tips_satisfaction'.
  $field_bases['field_arcs_tips_satisfaction'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_arcs_tips_satisfaction',
    'indexes' => array(),
    'locked' => 0,
    'module' => 'markup',
    'settings' => array(
      'markup' => array(
        'format' => 'full_html',
        'value' => '<h3>Not all your students are the same</h3><p>Some students will experience satisfaction</p><ul style="margin-left: 40px;"><li>just from the content of the information in the slides,<br>i.e. when the information matches their interest or because they got answers to questions they had before.</li><li>from the way in which information is represented,<br>e.g. from presentation that is elegant, concise, visual ...</li><li>from awareness of the progress they make/have made on a learning path</li><li>from a feeling that they ‘finally understand’ or from growing confidence</li><li>from successful completion of certain tasks and challenges</li><li>from working together with their peers</li></ul><h3>&nbsp;</h3><h3>Tips for providing satisfaction</h3><ul style="margin-left: 40px;"><li>Invest in elegant and concise formulations</li><li>Invest in diagrams</li><li>Invest in formulating assignments and tasks:<br>design closed questions that are perceived as authentic and relevant.<br>(Avoid assignments and tasks that stimulate blind guessing).</li><li>Provide feedback on progress</li><li>Provide challenging but realistic assignments at the end of a topic</li><li>Make explicit that students might benefit from studying your seamless eBook in a group and making the assignments together</li></ul>',
      ),
    ),
    'translatable' => 0,
    'type' => 'markup',
  );

  // Exported field_base: 'field_enable_student_arcs_rating'.
  $field_bases['field_enable_student_arcs_rating'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_enable_student_arcs_rating',
    'indexes' => array(
      'value' => array(
        0 => 'value',
      ),
    ),
    'locked' => 0,
    'module' => 'list',
    'settings' => array(
      'allowed_values' => array(
        0 => '',
        1 => '',
      ),
      'allowed_values_function' => '',
    ),
    'translatable' => 0,
    'type' => 'list_boolean',
  );

  // Exported field_base: 'field_image_credits'.
  $field_bases['field_image_credits'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_image_credits',
    'indexes' => array(
      'format' => array(
        0 => 'format',
      ),
    ),
    'locked' => 0,
    'module' => 'text',
    'settings' => array(),
    'translatable' => 0,
    'type' => 'text_long',
  );

  // Exported field_base: 'field_seamless_slides'.
  $field_bases['field_seamless_slides'] = array(
    'active' => 1,
    'cardinality' => -1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_seamless_slides',
    'indexes' => array(
      'target_id' => array(
        0 => 'target_id',
      ),
    ),
    'locked' => 0,
    'module' => 'entityreference',
    'settings' => array(
      'handler' => 'base',
      'handler_settings' => array(
        'behaviors' => array(
          'views-select-list' => array(
            'status' => 0,
          ),
        ),
        'sort' => array(
          'type' => 'none',
        ),
        'target_bundles' => array(
          'slide_text_image' => 'slide_text_image',
        ),
      ),
      'target_type' => 'node',
    ),
    'translatable' => 0,
    'type' => 'entityreference',
  );

  // Exported field_base: 'field_slide_image'.
  $field_bases['field_slide_image'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_slide_image',
    'indexes' => array(
      'fid' => array(
        0 => 'fid',
      ),
    ),
    'locked' => 0,
    'module' => 'image',
    'settings' => array(
      'default_image' => 0,
      'uri_scheme' => 'public',
    ),
    'translatable' => 0,
    'type' => 'image',
  );

  // Exported field_base: 'field_slide_paragraphs'.
  $field_bases['field_slide_paragraphs'] = array(
    'active' => 1,
    'cardinality' => 4,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_slide_paragraphs',
    'indexes' => array(),
    'locked' => 0,
    'module' => 'paragraphs',
    'settings' => array(),
    'translatable' => 0,
    'type' => 'paragraphs',
  );

  // Exported field_base: 'field_slide_paragraphs_2'.
  $field_bases['field_slide_paragraphs_2'] = array(
    'active' => 1,
    'cardinality' => 4,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_slide_paragraphs_2',
    'indexes' => array(),
    'locked' => 0,
    'module' => 'paragraphs',
    'settings' => array(),
    'translatable' => 0,
    'type' => 'paragraphs',
  );

  // Exported field_base: 'field_slide_question'.
  $field_bases['field_slide_question'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_slide_question',
    'indexes' => array(
      'target_id' => array(
        0 => 'target_id',
      ),
    ),
    'locked' => 0,
    'module' => 'entityreference',
    'settings' => array(
      'handler' => 'base',
      'handler_settings' => array(
        'behaviors' => array(
          'views-select-list' => array(
            'status' => 0,
          ),
        ),
        'sort' => array(
          'type' => 'none',
        ),
        'target_bundles' => array(
          'closedquestion' => 'closedquestion',
        ),
      ),
      'target_type' => 'node',
    ),
    'translatable' => 0,
    'type' => 'entityreference',
  );

  // Exported field_base: 'field_slide_template'.
  $field_bases['field_slide_template'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_slide_template',
    'indexes' => array(
      'value' => array(
        0 => 'value',
      ),
    ),
    'locked' => 0,
    'module' => 'list',
    'settings' => array(
      'allowed_values' => array(
        0 => 'Left 66% - Right 33%',
        1 => 'Left 33% - Right 66%',
        2 => 'Left 50% - Right 50%',
        4 => 'Top 100%  - Bottom 100%',
      ),
      'allowed_values_function' => '',
    ),
    'translatable' => 0,
    'type' => 'list_text',
  );

  // Exported field_base: 'field_slide_text'.
  $field_bases['field_slide_text'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_slide_text',
    'indexes' => array(
      'format' => array(
        0 => 'format',
      ),
    ),
    'locked' => 0,
    'module' => 'text',
    'settings' => array(),
    'translatable' => 0,
    'type' => 'text_long',
  );

  // Exported field_base: 'field_study_load'.
  $field_bases['field_study_load'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_study_load',
    'indexes' => array(),
    'locked' => 0,
    'module' => 'number',
    'settings' => array(),
    'translatable' => 0,
    'type' => 'number_integer',
  );

  return $field_bases;
}
