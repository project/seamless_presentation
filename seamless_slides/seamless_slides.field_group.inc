<?php

/**
 * @file
 * seamless_slides.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function seamless_slides_field_group_info() {
  $field_groups = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_arcs_explanation|node|slide_text_image|form';
  $field_group->group_name = 'group_arcs_explanation';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'slide_text_image';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_seamless_arcs';
  $field_group->data = array(
    'label' => 'Tips for stimulating student motivation',
    'weight' => '34',
    'children' => array(
      0 => 'field_arcs_explanation',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Tips for stimulating student motivation',
      'instance_settings' => array(
        'required_fields' => 1,
        'id' => '',
        'classes' => 'group-arcs-explanation field-group-fieldset',
        'description' => '',
      ),
      'formatter' => 'collapsed',
    ),
  );
  $field_groups['group_arcs_explanation|node|slide_text_image|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_arcs_relevance|node|slide_text_image|form';
  $field_group->group_name = 'group_arcs_relevance';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'slide_text_image';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_seamless_arcs';
  $field_group->data = array(
    'label' => 'Tips for raising awareness of relevance and for capturing attention',
    'weight' => '38',
    'children' => array(
      0 => 'field_arcs_tips_relevance',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Tips for raising awareness of relevance and for capturing attention',
      'instance_settings' => array(
        'required_fields' => 1,
        'id' => '',
        'classes' => 'group-arcs-relevance field-group-fieldset',
        'description' => '',
      ),
      'formatter' => 'collapsed',
    ),
  );
  $field_groups['group_arcs_relevance|node|slide_text_image|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_arcs_tips_attention|node|slide_text_image|form';
  $field_group->group_name = 'group_arcs_tips_attention';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'slide_text_image';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_seamless_arcs';
  $field_group->data = array(
    'label' => 'Tips for capturing attention.',
    'weight' => '36',
    'children' => array(
      0 => 'field_arcs_tips_attention',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Tips for capturing attention.',
      'instance_settings' => array(
        'required_fields' => 1,
        'id' => '',
        'classes' => 'group-arcs-tips-attention field-group-fieldset',
        'description' => '',
      ),
      'formatter' => 'collapsed',
    ),
  );
  $field_groups['group_arcs_tips_attention|node|slide_text_image|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_arcs_tips_confidence|node|slide_text_image|form';
  $field_group->group_name = 'group_arcs_tips_confidence';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'slide_text_image';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_seamless_arcs';
  $field_group->data = array(
    'label' => 'Tips to prevent undermining your student’s confidence',
    'weight' => '40',
    'children' => array(
      0 => 'field_arcs_tips_confidence',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Tips to prevent undermining your student’s confidence',
      'instance_settings' => array(
        'required_fields' => 1,
        'id' => '',
        'classes' => 'group-arcs-tips-confidence field-group-fieldset',
        'description' => '',
      ),
      'formatter' => 'collapsed',
    ),
  );
  $field_groups['group_arcs_tips_confidence|node|slide_text_image|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_arcs_tips_satisfaction|node|slide_text_image|form';
  $field_group->group_name = 'group_arcs_tips_satisfaction';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'slide_text_image';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_seamless_arcs';
  $field_group->data = array(
    'label' => 'Tips for providing satisfaction',
    'weight' => '42',
    'children' => array(
      0 => 'field_arcs_tips_satisfaction',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Tips for providing satisfaction',
      'instance_settings' => array(
        'required_fields' => 1,
        'id' => '',
        'classes' => 'group-arcs-tips-satisfaction field-group-fieldset',
        'description' => '',
      ),
      'formatter' => 'collapsed',
    ),
  );
  $field_groups['group_arcs_tips_satisfaction|node|slide_text_image|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_seamless_arcs|node|slide_text_image|form';
  $field_group->group_name = 'group_seamless_arcs';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'slide_text_image';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Assess motivational aspects of slide',
    'weight' => '5',
    'children' => array(
      0 => 'field_arcs_attention',
      1 => 'field_arcs_relevance',
      2 => 'field_arcs_confidence',
      3 => 'field_arcs_satisfaction',
      4 => 'group_arcs_tips_attention',
      5 => 'group_arcs_explanation',
      6 => 'group_arcs_relevance',
      7 => 'group_arcs_tips_confidence',
      8 => 'group_arcs_tips_satisfaction',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Assess motivational aspects of slide',
      'instance_settings' => array(
        'required_fields' => 1,
        'id' => '',
        'classes' => 'group-seamless-arcs field-group-fieldset',
        'description' => 'Use this form to assess whether this slide will be motivating to your students to learn the topics discussed. See <a href="https://www.google.com/search?q=arcs+keller" target="_blank">ARCS Model of Motivational Design (Keller)</a>.',
      ),
      'formatter' => 'collapsed',
    ),
  );
  $field_groups['group_seamless_arcs|node|slide_text_image|form'] = $field_group;

  // Translatables
  // Included for use with string extractors like potx.
  t('Assess motivational aspects of slide');
  t('Tips for capturing attention.');
  t('Tips for providing satisfaction');
  t('Tips for raising awareness of relevance and for capturing attention');
  t('Tips for stimulating student motivation');
  t('Tips to prevent undermining your student’s confidence');

  return $field_groups;
}
