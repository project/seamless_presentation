<?php

/**
 * @file
 * seamless_slides.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function seamless_slides_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance:
  // 'node-seamless_presentation-field_enable_student_arcs_rating'.
  $field_instances['node-seamless_presentation-field_enable_student_arcs_rating'] = array(
    'bundle' => 'seamless_presentation',
    'default_value' => array(
      0 => array(
        'value' => 0,
      ),
    ),
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 1,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_enable_student_arcs_rating',
    'label' => 'Enable ARCS rating for student',
    'required' => 0,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(
        'display_label' => 1,
      ),
      'type' => 'options_onoff',
      'weight' => -3,
    ),
  );

  // Exported field_instance:
  // 'node-seamless_presentation-field_seamless_slides'.
  $field_instances['node-seamless_presentation-field_seamless_slides'] = array(
    'bundle' => 'seamless_presentation',
    'deleted' => 0,
    'description' => 'Select and order the slides.<br>To remove a slide: unselect checkbox in list and save the presentation.',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_seamless_slides',
    'label' => 'Seamless Slides',
    'required' => 0,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'entityreference_view_widget',
      'settings' => array(
        'allow_duplicates' => 0,
        'close_modal' => 1,
        'pass_argument' => 1,
        'pass_arguments' => '',
        'rendered_entity' => 0,
        'view' => 'seamless_slides_reference_view|entityreference_view_widget_seamless',
        'view_mode' => 'full',
      ),
      'type' => 'entityreference_view_widget',
      'weight' => -4,
    ),
  );

  // Exported field_instance: 'node-slide_text_image-field_arcs_attention'.
  $field_instances['node-slide_text_image-field_arcs_attention'] = array(
    'bundle' => 'slide_text_image',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 11,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_arcs_attention',
    'label' => 'Does this slide help to capture the student\'s attention?',
    'required' => 0,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(),
      'type' => 'options_buttons',
      'weight' => 35,
    ),
  );

  // Exported field_instance: 'node-slide_text_image-field_arcs_confidence'.
  $field_instances['node-slide_text_image-field_arcs_confidence'] = array(
    'bundle' => 'slide_text_image',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 13,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_arcs_confidence',
    'label' => 'Does this slide help to prevent undermining the confidence of my students?',
    'required' => 0,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(),
      'type' => 'options_buttons',
      'weight' => 39,
    ),
  );

  // Exported field_instance: 'node-slide_text_image-field_arcs_explanation'.
  $field_instances['node-slide_text_image-field_arcs_explanation'] = array(
    'bundle' => 'slide_text_image',
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'markup',
        'settings' => array(),
        'type' => 'markup_default',
        'weight' => 16,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_arcs_explanation',
    'label' => 'ARCS explanation',
    'required' => 0,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 0,
      'module' => 'markup',
      'settings' => array(),
      'type' => 'markup',
      'weight' => 35,
    ),
  );

  // Exported field_instance: 'node-slide_text_image-field_arcs_relevance'.
  $field_instances['node-slide_text_image-field_arcs_relevance'] = array(
    'bundle' => 'slide_text_image',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 12,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_arcs_relevance',
    'label' => 'Does this slide my students aware of the relevance of what it presents?',
    'required' => 0,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(),
      'type' => 'options_buttons',
      'weight' => 37,
    ),
  );

  // Exported field_instance: 'node-slide_text_image-field_arcs_satisfaction'.
  $field_instances['node-slide_text_image-field_arcs_satisfaction'] = array(
    'bundle' => 'slide_text_image',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 14,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_arcs_satisfaction',
    'label' => 'Does this slide provide my students with satisfying learning experiences?',
    'required' => 0,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(),
      'type' => 'options_buttons',
      'weight' => 41,
    ),
  );

  // Exported field_instance: 'node-slide_text_image-field_arcs_tips_attention'.
  $field_instances['node-slide_text_image-field_arcs_tips_attention'] = array(
    'bundle' => 'slide_text_image',
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'markup',
        'settings' => array(),
        'type' => 'markup_default',
        'weight' => 15,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_arcs_tips_attention',
    'label' => 'ARCS Tips attention',
    'required' => 0,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 0,
      'module' => 'markup',
      'settings' => array(),
      'type' => 'markup',
      'weight' => 25,
    ),
  );

  // Exported field_instance:
  // 'node-slide_text_image-field_arcs_tips_confidence'.
  $field_instances['node-slide_text_image-field_arcs_tips_confidence'] = array(
    'bundle' => 'slide_text_image',
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'markup',
        'settings' => array(),
        'type' => 'markup_default',
        'weight' => 18,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_arcs_tips_confidence',
    'label' => 'ARCS tips confidence',
    'required' => 0,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 0,
      'module' => 'markup',
      'settings' => array(),
      'type' => 'markup',
      'weight' => 42,
    ),
  );

  // Exported field_instance: 'node-slide_text_image-field_arcs_tips_relevance'.
  $field_instances['node-slide_text_image-field_arcs_tips_relevance'] = array(
    'bundle' => 'slide_text_image',
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'markup',
        'settings' => array(),
        'type' => 'markup_default',
        'weight' => 17,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_arcs_tips_relevance',
    'label' => 'ARCS tips relevance',
    'required' => 0,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 0,
      'module' => 'markup',
      'settings' => array(),
      'type' => 'markup',
      'weight' => 40,
    ),
  );

  // Exported field_instance:
  // 'node-slide_text_image-field_arcs_tips_satisfaction'.
  $field_instances['node-slide_text_image-field_arcs_tips_satisfaction'] = array(
    'bundle' => 'slide_text_image',
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'markup',
        'settings' => array(),
        'type' => 'markup_default',
        'weight' => 19,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_arcs_tips_satisfaction',
    'label' => 'ARCS tips satisfaction',
    'required' => 0,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 0,
      'module' => 'markup',
      'settings' => array(),
      'type' => 'markup',
      'weight' => 43,
    ),
  );

  // Exported field_instance: 'node-slide_text_image-field_slide_paragraphs'.
  $field_instances['node-slide_text_image-field_slide_paragraphs'] = array(
    'bundle' => 'slide_text_image',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'Select an item type to add.',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 4,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_slide_paragraphs',
    'label' => 'Add Slide items',
    'required' => 1,
    'settings' => array(
      'add_mode' => 'button',
      'allowed_bundles' => array(
        'seamless_image' => 'seamless_image',
        'seamless_question' => 'seamless_question',
        'seamless_text' => 'seamless_text',
      ),
      'bundle_weights' => array(
        'seamless_image' => -5,
        'seamless_question' => -4,
        'seamless_text' => -6,
      ),
      'default_edit_mode' => 'preview',
      'title' => 'Slide item',
      'title_multiple' => 'Slide items',
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 0,
      'module' => 'paragraphs',
      'settings' => array(),
      'type' => 'paragraphs_embed',
      'weight' => 2,
    ),
  );

  // Exported field_instance: 'node-slide_text_image-field_slide_paragraphs_2'.
  $field_instances['node-slide_text_image-field_slide_paragraphs_2'] = array(
    'bundle' => 'slide_text_image',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'Select an item type to add.',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 5,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_slide_paragraphs_2',
    'label' => 'Add slide items',
    'required' => 1,
    'settings' => array(
      'add_mode' => 'button',
      'allowed_bundles' => array(
        'seamless_image' => 'seamless_image',
        'seamless_question' => 'seamless_question',
        'seamless_text' => 'seamless_text',
      ),
      'bundle_weights' => array(
        'seamless_image' => -5,
        'seamless_question' => -4,
        'seamless_text' => -6,
      ),
      'default_edit_mode' => 'preview',
      'title' => 'Slide item',
      'title_multiple' => 'Slide items',
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 0,
      'module' => 'paragraphs',
      'settings' => array(),
      'type' => 'paragraphs_embed',
      'weight' => 3,
    ),
  );

  // Exported field_instance: 'node-slide_text_image-field_slide_template'.
  $field_instances['node-slide_text_image-field_slide_template'] = array(
    'bundle' => 'slide_text_image',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'Select slide layout (form will update after saving).',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_slide_template',
    'label' => 'Slide Layout Template',
    'required' => 0,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(),
      'type' => 'options_select',
      'weight' => 1,
    ),
  );

  // Exported field_instance: 'node-slide_text_image-field_study_load'.
  $field_instances['node-slide_text_image-field_study_load'] = array(
    'bundle' => 'slide_text_image',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'Estimated average study load (in seconds).',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 6,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_study_load',
    'label' => 'Study load',
    'required' => 0,
    'settings' => array(
      'max' => '',
      'min' => '',
      'prefix' => '',
      'suffix' => 'seconds',
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 0,
      'module' => 'number',
      'settings' => array(),
      'type' => 'number',
      'weight' => 4,
    ),
  );

  // Exported field_instance:
  // 'paragraphs_item-seamless_image-field_image_credits'.
  $field_instances['paragraphs_item-seamless_image-field_image_credits'] = array(
    'bundle' => 'seamless_image',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 1,
      ),
      'paragraphs_editor_preview' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'paragraphs_item',
    'field_name' => 'field_image_credits',
    'label' => 'Image Copyrights/Attribution',
    'required' => 0,
    'settings' => array(
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'rows' => 1,
      ),
      'type' => 'text_textarea',
      'weight' => 3,
    ),
  );

  // Exported field_instance:
  // 'paragraphs_item-seamless_image-field_slide_image'.
  $field_instances['paragraphs_item-seamless_image-field_slide_image'] = array(
    'bundle' => 'seamless_image',
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'module' => 'file_entity',
        'settings' => array(
          'file_view_mode' => 'default',
        ),
        'type' => 'file_rendered',
        'weight' => 0,
      ),
      'paragraphs_editor_preview' => array(
        'label' => 'above',
        'module' => 'file_entity',
        'settings' => array(
          'file_view_mode' => 'preview',
        ),
        'type' => 'file_rendered',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'paragraphs_item',
    'field_name' => 'field_slide_image',
    'label' => 'Slide Media',
    'required' => 0,
    'settings' => array(
      'alt_field' => 0,
      'default_image' => 0,
      'file_directory' => '',
      'file_extensions' => 'png gif jpg jpeg',
      'max_filesize' => '',
      'max_resolution' => '',
      'min_resolution' => '',
      'title_field' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'media',
      'settings' => array(
        'allowed_schemes' => array(
          'oembed' => 0,
          'public' => 'public',
          'youtube' => 'youtube',
        ),
        'allowed_types' => array(
          'audio' => 0,
          'document' => 0,
          'image' => 'image',
          'video' => 'video',
        ),
        'browser_plugins' => array(
          'media_default--media_browser_1' => 0,
          'media_default--media_browser_my_files' => 0,
          'media_internet' => 0,
          'upload' => 0,
        ),
      ),
      'type' => 'media_generic',
      'weight' => 2,
    ),
  );

  // Exported field_instance:
  // 'paragraphs_item-seamless_question-field_slide_question'.
  $field_instances['paragraphs_item-seamless_question-field_slide_question'] = array(
    'bundle' => 'seamless_question',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'module' => 'entityreference',
        'settings' => array(
          'bypass_access' => FALSE,
          'link' => FALSE,
        ),
        'type' => 'entityreference_label',
        'weight' => 0,
      ),
      'paragraphs_editor_preview' => array(
        'label' => 'above',
        'module' => 'entityreference',
        'settings' => array(
          'bypass_access' => FALSE,
          'link' => FALSE,
        ),
        'type' => 'entityreference_label',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'paragraphs_item',
    'field_name' => 'field_slide_question',
    'label' => 'Slide Question',
    'required' => 0,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'entityreference_view_widget',
      'settings' => array(
        'allow_duplicates' => 0,
        'close_modal' => 1,
        'pass_argument' => 1,
        'pass_arguments' => '',
        'rendered_entity' => 0,
        'view' => 'select_closedquestion_for_slide|entityreference_view_widget_cq',
        'view_mode' => 'full',
      ),
      'type' => 'entityreference_view_widget',
      'weight' => 2,
    ),
  );

  // Exported field_instance: 'paragraphs_item-seamless_text-field_slide_text'.
  $field_instances['paragraphs_item-seamless_text-field_slide_text'] = array(
    'bundle' => 'seamless_text',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 0,
      ),
      'paragraphs_editor_preview' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'paragraphs_item',
    'field_name' => 'field_slide_text',
    'label' => 'Slide Text',
    'required' => 0,
    'settings' => array(
      'text_processing' => 1,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'rows' => 5,
      ),
      'type' => 'text_textarea',
      'weight' => 2,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('ARCS Tips attention');
  t('ARCS explanation');
  t('ARCS tips confidence');
  t('ARCS tips relevance');
  t('ARCS tips satisfaction');
  t('Add Slide items');
  t('Add slide items');
  t('Does this slide help to capture the student\'s attention?');
  t('Does this slide help to prevent undermining the confidence of my students?');
  t('Does this slide my students aware of the relevance of what it presents?');
  t('Does this slide provide my students with satisfying learning experiences?');
  t('Enable ARCS rating for student');
  t('Estimated average study load (in seconds).');
  t('Image Copyrights/Attribution');
  t('Seamless Slides');
  t('Select an item type to add.');
  t('Select and order the slides.<br>To remove a slide: unselect checkbox in list and save the presentation.');
  t('Select slide layout (form will update after saving).');
  t('Slide Layout Template');
  t('Slide Media');
  t('Slide Question');
  t('Slide Text');
  t('Study load');

  return $field_instances;
}
