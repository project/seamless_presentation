<?php

/**
 * @file
 * seamless_slides.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function seamless_slides_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
  if ($module == "page_manager" && $api == "pages_default") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function seamless_slides_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function seamless_slides_node_info() {
  $items = array(
    'seamless_presentation' => array(
      'name' => t('Seamless presentation'),
      'base' => 'node_content',
      'description' => t('Organize Seamless slides into a presentation.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'slide_text_image' => array(
      'name' => t('Seamless Slide'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}

/**
 * Implements hook_paragraphs_info().
 */
function seamless_slides_paragraphs_info() {
  $items = array(
    'seamless_image' => array(
      'name' => 'Media',
      'bundle' => 'seamless_image',
      'locked' => '1',
    ),
    'seamless_question' => array(
      'name' => 'Question',
      'bundle' => 'seamless_question',
      'locked' => '1',
    ),
    'seamless_text' => array(
      'name' => 'Text',
      'bundle' => 'seamless_text',
      'locked' => '1',
    ),
  );
  return $items;
}
