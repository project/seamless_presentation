<?php

/**
 * @file
 * Plugin definition for 100% layout.
 */

// Plugin definition.
$plugin = array(
  'title' => t('Seamless: Center 100%'),
  'category' => t('Seamless Layouts'),
  'icon' => 'seamless_onecol_stacked_100.png',
  'theme' => 'seamless_onecol_stacked',
  'css' => 'seamless_onecol_stacked_100.css',
  'regions' => array(
    'top' => t('Top'),
    'left' => t('Center'),
    'bottom' => t('Bottom'),
  ),
);
