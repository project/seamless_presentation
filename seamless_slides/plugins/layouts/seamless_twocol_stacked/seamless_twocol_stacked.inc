<?php

/**
 * @file
 * Plugin definition for 50% - 50% layout.
 */

// Plugin definition.
$plugin = array(
  'title' => t('Seamless: Left 50% - Right 50%'),
  'category' => t('Seamless Layouts'),
  'icon' => 'seamless_twocol_stacked.png',
  'theme' => 'seamless_twocol_stacked',
  'css' => 'seamless_twocol_stacked.css',
  'regions' => array(
    'top' => t('Top'),
    'left' => t('Left'),
    'right' => t('Right'),
    'bottom' => t('Bottom'),
  ),
);
