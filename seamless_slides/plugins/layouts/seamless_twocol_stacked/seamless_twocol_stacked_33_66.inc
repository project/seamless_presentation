<?php

/**
 * @file
 * Plugin definition for 33% - 66% layout.
 */

// Plugin definition.
$plugin = array(
  'title' => t('Seamless: Left 33% - Right 66%'),
  'category' => t('Seamless Layouts'),
  'icon' => 'seamless_twocol_stacked_33_66.png',
  'theme' => 'seamless_twocol_stacked',
  'css' => array('seamless_twocol_stacked.css', 'seamless_twocol_stacked_33_66.css'),
  'regions' => array(
    'top' => t('Top'),
    'left' => t('Left'),
    'right' => t('Right'),
    'bottom' => t('Bottom'),
  ),
);
