<?php

/**
 * @file
 * Plugin definition for 66% - 33% layout.
 */

// Plugin definition.
$plugin = array(
  'title' => t('Seamless: Left 66% - Right 33%'),
  'category' => t('Seamless Layouts'),
  'icon' => 'seamless_twocol_stacked_66_33.png',
  'theme' => 'seamless_twocol_stacked',
  'css' => array('seamless_twocol_stacked.css', 'seamless_twocol_stacked_66_33.css'),
  'regions' => array(
    'top' => t('Top'),
    'left' => t('Left'),
    'right' => t('Right'),
    'bottom' => t('Bottom'),
  ),
);
